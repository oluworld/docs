---
layout: default
title: Home
nav_order: 1
description: "OluWorld! is a colection of really old software."
permalink: /
---

# Explore the early 2000's with me
{: .fs-9 }

OluWorld! is a collection of software I was writing in the early 2000's in Python 2.0 and SmallEiffel with lcc.  I am updating it for Python 2.7 (and eventually 3.9) and SmartEiffel 2.4 (the other Eiffel compilers I don't like.)
{: .fs-6 .fw-300 }

---

## Getting started

### Dependencies

Download Python 2.7 and SmartEiffel 2.4.  Depending on what happens with my coding adventures you may also need wxPython 3.0.

### Configure Just the Docs

- [See configuration options]({{ site.baseurl }}{% link docs/configuration.md %})

---

## Subprojects

### Servers

[oluworld-nntp-server](https://gitlab.com/oluworld/oluworld-nntp-server) <br/>
[oluworld-pop-server](https://gitlab.com/oluworld/oluworld-pop-server) <br/>
[oluworld-smtp-server](https://gitlab.com/oluworld/oluworld-smtp-server)

### Apps

[GetPop/Mail 1-8](https://gitlab.com/oluworld/getpop) <br/>
[GetNews2Onet](https://gitlab.com/oluworld/getnews2onet) <br/>
[GetNews](https://gitlab.com/oluworld/getnews)

### AppWorks Demos

[POPFolderDemo](https://gitlab.com/oluworld/popfolderdemo) <br/>
[DBiTests](https://gitlab.com/oluworld/dbitests)

### AppWorks

[AppWorks](https://gitlab.com/oluworld/appworks)

### Games

[NibblesCountry](https://gitlab.com/oluworld/nibblescountry) <br/>
[Nibbles/Java](https://gitlab.com/oluworld/nibbles-j)

### Programming Languages

[Alvo](https://gitlab.com/oluworld/alvo-j) <br/>
[Qiameth](https://gitlab.com/oluworld/qiameth) <br/>
[SmartEiffel-mod](https://gitlab.com/oluworld/smarteiffel-mod) <br/>

### Other (Recent)

[Confabulator](https://gitlab.com/oluworld/confabulator) <br/>
[Browser](https://gitlab.com/oluworld/browser)

## About the project

OluWorld! is &copy; 2017-{{ "now" | date: "%Y" }} by [the author](mailto:oluoluolu-REMOVE@protonmail.com).

### License

This software is distributed by an [LGPL license](https://gitlab.com/oluworld/oluworld.github.io/tree/master/LICENSE.LGPL).

### Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. Read more 
about becoming a contributor in [our GitLab repo](https://github.com/pmarsceill/just-the-docs#contributing).

### Code of Conduct

OluWorld! is committed to fostering a welcoming community.

[View our Code of Conduct](https://github.com/pmarsceill/just-the-docs/tree/master/CODE_OF_CONDUCT.md) on our GitHub repository.
